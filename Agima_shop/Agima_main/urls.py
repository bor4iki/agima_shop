from django.urls import path

from .views import main_page, basket, by_brand, ProductCreateView

urlpatterns = [
    path('', main_page, name='main_page'),
    path('basket/', basket, name='basket'),
    path('add/', ProductCreateView.as_view(), name='add'),
    path('<int:brand_id>/', by_brand, name='by_brand')
]
