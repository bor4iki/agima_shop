from django.db import models

# Create your models here.


class Product(models.Model):
    title = models.CharField(max_length=50, verbose_name='Модель')
    content = models.TextField(null=True, blank=True, verbose_name='Описание')
    price = models.FloatField(verbose_name='Цена')
    image = models.ImageField(upload_to='article/', null=True, blank=True, verbose_name='Картинка')
    published = models.DateTimeField(auto_now_add=True, db_index=True,
                                     verbose_name='Опубликовано')
    brand = models.ForeignKey('Brand', null=True, on_delete=models.PROTECT, verbose_name='Бренды')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = 'Товары'
        verbose_name = 'Товар'
        ordering = ['-published']


class Brand(models.Model):
    name = models.CharField(max_length=20, db_index=True, verbose_name='Бренды')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Бренды'
        verbose_name = 'Бренд'
        ordering = ['name']
