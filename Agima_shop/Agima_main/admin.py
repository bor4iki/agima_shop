from django.contrib import admin

from .models import Product, Brand
# Register your models here.


class ProductAdmin(admin.ModelAdmin):
    list_display = ('title', 'content', 'published', 'brand')
    list_display_links = ('title', 'content')
    search_fields = ('title',)
    list_filter = ['brand', ]


admin.site.register(Product, ProductAdmin)
admin.site.register(Brand)
