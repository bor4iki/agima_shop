from django.apps import AppConfig


class AgimaMainConfig(AppConfig):
    name = 'Agima_main'
