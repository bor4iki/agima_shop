from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic.edit import CreateView
from .models import Brand, Product
from .forms import ProductForm


# Create your views here.

def by_brand(request, brand_id):
    names = Product.objects.filter(brand=brand_id)
    brands = Brand.objects.all()
    current_brand = Brand.objects.get(pk=brand_id)
    context = {'names': names, 'brands': brands, 'current_brand': current_brand}
    return render(request, "Agima_main/by_rubric.html", context)


def main_page(request):
    names = Product.objects.all()
    brands = Brand.objects.all()
    price = Product.objects.all()
    context = {'names': names, 'brands': brands, 'price': price, }
    return render(request, "Agima_main/main_page.html", context)


def basket(request):
    return render(request, "Agima_main/basket.html", )


class ProductCreateView(CreateView):
    template_name = 'Agima_main/create.html'
    form_class = ProductForm
    success_url = reverse_lazy('main_page')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['brand'] = Brand.objects.all()
        return context
